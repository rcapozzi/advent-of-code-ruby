#!/usr/bin/env ruby

require 'set'

file_path = File.expand_path("../day-15-input.txt", __FILE__)
input     = File.read(file_path)

rows = input.split("\n")

map = Array.new(rows.length) { Array.new(rows.first.length) }

goblins = []
elfs = []
units = []

rows.each.with_index do |row, y|
  row.chars.each.with_index do |cell, x|
    case cell
    when ?.
      map[y][x] = cell
    when ?G
      goblins << { x: x, y: y, hp: 200, goblin: true }
      units << goblins.last
      map[y][x] = '.'
    when ?E
      elfs << { x: x, y: y, hp: 200, goblin: false }
      units << elfs.last
      map[y][x] = '.'
    when ?#
      map[y][x] = cell
    end
  end
end

puts map.map(&:join).join("\n")

puts units

round = 0

def reachable(x, y, x1, y1)

end

def shortest_path(x, y, x1, y1)
  solutions = []

  stack = []
  visited = Set.new
  visited.add([x, y])

  stack.concat [x, y]

  while solutions.empty?
    x2, y2 = stack.shift
    if x2 == x1 && y2 == y1
      solutions <<
    [[x2, y2-1], [x2-1, y2], [x2+1, y2], [x2, y2+1]]
  end

  puts solutions
  solutions
end

10.times do
  round += 1

  units.sort_by! { |unit| [unit[:y], unit[:x]] }

  units.each do |unit|
    targets = units.select { |u| u[:goblin] != unit[:goblin] }

    # in range
    range = targets.map { |target| [target[:x], target[:y]] }
      .map { |(x, y)| [[x+1, y], [x-1, y], [x, y+1], [x, y-1]] }
      .flatten(1)
      .reject { |(x, y)| map[y][x] == "#" }
      .reject { |(x, y)| units.find { |u| u[:x] == x && u[:y] == y } }
      .reject { |(x, y)| reachable(unit[:x], unit[:y], x, y) }
    puts range.inspect

    # reachable

  end
end